FROM        centos:centos6
MAINTAINER  benson
#ENV         LANG C.UTF-8
RUN        yum -y install wget && \ 
cd /etc/yum.repos.d && \
rpm --import http://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-6  && \
rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm  && \
rpm --import http://dl.iuscommunity.org/pub/ius/IUS-COMMUNITY-GPG-KEY  && \
rpm -ivh http://dl.iuscommunity.org/pub/ius/stable/Redhat/6/x86_64/ius-release-1.0-11.ius.el6.noarch.rpm  && \
#mv CentOS-Base.repo CentOS-Base.repo.bak && \
#wget http://mirrors.163.com/.help/CentOS6-Base-163.repo && \
#mv CentOS6-Base-163.repo CentOS-Base.repo && \
yum -y install mysql-server mysql mysql-devel && \
yum -y install bc  && \
yum -y install patch which php-mysql php-gd php-imap php-ldap php-odbc php-pear php-xml php-xmlrpc && \
yum -y install php && \
yum -y install php-mcrypt php-common php-mysql php-pear php-devel httpd-devel pcre-devel gcc make && \
rm -rf /var/cache/yum/* && yum clean all 
RUN sed -i -e 's~^;date.timezone =$~date.timezone = Asia/Shanghai~g' /etc/php.ini
#pecl install apc uploadprogress && \
#echo "extension=apc.so" > /etc/php.d/apc.ini && \
#echo "extension=uploadprogress.so" > /etc/php.d/uploadprogress.ini && \
ADD         image-common /tmp/dexec/image-common
VOLUME      /tmp/dexec/build
ENTRYPOINT  ["/tmp/dexec/image-common/dexec-script.sh", "php"]